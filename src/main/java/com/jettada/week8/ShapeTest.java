package com.jettada.week8;

public class ShapeTest {
    public static void main(String[] args) {
        RectangleClass rect1 = new RectangleClass("rect1",10,5);
        rect1.getName();
        rect1.calArea();
        rect1.print();
        rect1.printPerimeter();
        RectangleClass rect2 = new RectangleClass("rect2",5,3);
        rect2.getName();
        rect2.calArea();
        rect2.print();
        rect2.printPerimeter();
        CircleClass circle1 = new CircleClass("circle1",1);
        circle1.print();
        CircleClass circle2 = new CircleClass("circle2",2);
        circle2.print();
        TriangleClass triangle = new TriangleClass("triangle",5,5,6);
        triangle.print();
    }
}
