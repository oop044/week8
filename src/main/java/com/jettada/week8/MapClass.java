package com.jettada.week8;

public class MapClass {
    private String name;
    private int width;
    private int height;

    public MapClass(String name, int width, int height) {
        this.name = name;
        this.width = width;
        this.height = height;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public void printMap() {
        for (int y = 1; y <= height; y++) {
            for (int x = 1; x <= width; x++) {
                System.out.print("-");
            }
            System.out.println();
        }
    }

}
