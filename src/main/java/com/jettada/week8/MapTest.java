package com.jettada.week8;

import java.util.jar.Attributes.Name;

public class MapTest {
    public static void main(String[] args) {
        MapClass map = new MapClass("Map1",5,5);
        System.out.println(map.getName() + " width = " + map.getWidth() + " height = "+ map.getHeight());
        map.printMap();
        MapClass map2 = new MapClass("Map2",10,10);
        System.out.println(map2.getName() + " width = " + map2.getWidth() + " height = "+ map2.getHeight());
        map2.printMap();
    }
}
