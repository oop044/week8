package com.jettada.week8;

public class CircleClass {
    private String name;
    private double redise;

    public CircleClass(String name, double redise) {
        this.name = name;
        this.redise = redise;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public double circleArea() {
        double ra = Math.PI * (redise * redise);
        return ra;
    }

    public double circlePerimeter() {
        double pari = 2 * (22 / 7) * redise;
        return pari;
    }

    public void print() {
        System.out.println(name + ":Area : " + circleArea());
        System.out.println(name + ":Perimeter : " + circlePerimeter());
    }
}
