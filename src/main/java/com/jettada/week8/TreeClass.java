package com.jettada.week8;

public class TreeClass {
    private String name;
    private int x ;
    private int y ;
    public TreeClass(String name , int x, int y){
        this.name = name;
        this.x = x;
        this.y = y;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
    public void print(){
        System.out.println(name + " X:"+ x + " Y:" + y);
    }
}
