package com.jettada.week8;

public class TriangleClass {
    private String name;
    private double A;
    private double B;
    private double C;
    public TriangleClass(String name, double A , double B, double C){
        this.name = name;
        this.A = A;
        this.B = B;
        this.C = C;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
    public double TriangleArea() {
        double s = (A + B + C) / 2;
        s = s * (s - A) * (s - B) * (s - C);
        s = Math.sqrt(s);
        return s;
    }
    public double TrianglePerimeter() {
        return A + B + C;
    }
    public void print(){
        System.out.println(name + " calArea : " + TriangleArea());
        System.out.println(name + " calPerimeter : " + TrianglePerimeter());
    }
}
