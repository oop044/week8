package com.jettada.week8;

public class RectangleClass {
    private String name ;
    private double width ;
    private double heigh  ;
    public RectangleClass(String name ,double width, double heigh){
        this.name = name;
        this.width = width;
        this.heigh = heigh;
    }
    public void setName(String name){
        this.name = name;
    }
    public String getName(){
        return name ;
    }
    public double calArea(){
        double area = width * heigh;
        return area;
    }
    public void print(){
        System.out.println(name + ":Area: " +calArea());
    }
    public double Perimeter(){
        return (width + heigh)*2;
    }
    public void printPerimeter(){
        System.out.println(name + ":Perimeter: " +Perimeter());
    }
    
}
